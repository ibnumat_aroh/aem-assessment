import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root', 
})
export class AuthService {

  constructor(private http:HttpClient) { }

  public login(user: User): Observable<any> {
    return this.http.post<any>(
      'http://test-demo.aemenersol.com/api/account/login', 
      user
    );
  }
}
